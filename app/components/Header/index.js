import React from 'react';

import A from './A';
import Img from './Img';

// Related svg icon import
import IconSearch from '../SvgIcons/IconSearch';
import IconChat from '../SvgIcons/IconChat';
import IconBell from '../SvgIcons/IconBell';

// Import css
import './css/header.min.css';

function Header() {
    // eslint-disable-next-line no-unused-vars
    const [open, setOpen] = React.useState(false);

    // eslint-disable-next-line no-unused-vars
    const handleClickOpen = () => {
        setOpen(true);
    };

    // eslint-disable-next-line no-unused-vars
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div className="coremainnav">
            <div className="logosec">
                <A href="#">
                    <Img src="//via.placeholder.com/100x50" alt="Sample" />
                </A>
            </div>
            <div className="centercol">
                <div className="searchwrap">
                    <input type="text" placeholder="Search keyword" />
                    <IconSearch />
                </div>
            </div>

            <div className="optionsright">
                <div className="otheropts">
                    <div className="sinicon">
                        <div className="btnwrap">
                            <IconChat />
                        </div>
                    </div>
                    <div className="sinicon">
                        <div className="btnwrap">
                            <IconBell />
                        </div>
                    </div>
                </div>

                <div className="userimg">
                    <A href="#">
                        <Img src="//via.placeholder.com/100x100" alt="Sample" />
                    </A>
                </div>
            </div>

            {/* 		
      <A href="https://www.reactboilerplate.com/">
        This is header 
      </A>

      <NavBar>

        <HeaderLink to="/">
          <FormattedMessage {...messages.home} />
        </HeaderLink>
        <HeaderLink to="/features">
          <FormattedMessage {...messages.features} />
        </HeaderLink>
        <HeaderLink to="/customers">
          <FormattedMessage {...messages.customers} />
        </HeaderLink>
        <HeaderLink to="/new-product">
          <FormattedMessage {...messages.newProduct} />
        </HeaderLink>
        <HeaderLink to="/order-details">
          <FormattedMessage {...messages.orderDetails} />
        </HeaderLink>
        <HeaderLink to="/orders">
          <FormattedMessage {...messages.orders} />
        </HeaderLink>
        <HeaderLink to="/preferences">
          <FormattedMessage {...messages.preferences} />
        </HeaderLink>
        <HeaderLink to="/products">
          <FormattedMessage {...messages.products} />
        </HeaderLink>
        <HeaderLink to="/reports">
          <FormattedMessage {...messages.reports} />
        </HeaderLink>
        <HeaderLink to="/settings">
          <FormattedMessage {...messages.settings} />
        </HeaderLink>
        <HeaderLink to="/themes">
            
        </HeaderLink>
      </NavBar> */}
        </div>
    );
}

export default Header;
