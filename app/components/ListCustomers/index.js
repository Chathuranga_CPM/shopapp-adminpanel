import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import MaterialTable from 'material-table';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));

export default function EnhancedTable() {
    const classes = useStyles();
    const [dense, setDense] = React.useState(false);

    const handleChangeDense = event => {
        setDense(event.target.checked);
    };
    return (
        <div className={classes.root}>
            <MaterialTable
                title="Editable Preview"
                columns={[
                    { title: 'Name', field: 'name' },
                    {
                        title: 'Surname',
                        field: 'surname',
                        initialEditValue: 'initial edit value',
                    },
                    {
                        title: 'Birth Year',
                        field: 'birthYear',
                        type: 'numeric',
                    },
                    {
                        title: 'Birth Place',
                        field: 'birthCity',
                        lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' },
                    },
                ]}
                data={[
                    {
                        name: 'Mehmet',
                        surname: 'Baran',
                        birthYear: 1987,
                        birthCity: 63,
                    },
                    {
                        name: 'Zerya Betül',
                        surname: 'Baran',
                        birthYear: 2017,
                        birthCity: 34,
                    },
                ]}
                editable={{
                    onRowAdd: newData =>
                        // eslint-disable-next-line no-unused-vars
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    const { data } = this.state;
                                    data.push(newData);
                                    this.setState({ data }, () => resolve());
                                }
                                resolve();
                            }, 1000);
                        }),
                    onRowUpdate: (newData, oldData) =>
                        // eslint-disable-next-line no-unused-vars
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    const { data } = this.state;
                                    const index = data.indexOf(oldData);
                                    data[index] = newData;
                                    this.setState({ data }, () => resolve());
                                }
                                resolve();
                            }, 1000);
                        }),
                    onRowDelete: oldData =>
                        // eslint-disable-next-line no-unused-vars
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    const { data } = this.state;
                                    const index = data.indexOf(oldData);
                                    data.splice(index, 1);
                                    this.setState({ data }, () => resolve());
                                }
                                resolve();
                            }, 1000);
                        }),
                }}
            />
            <FormControlLabel
                control={
                    <Switch checked={dense} onChange={handleChangeDense} />
                }
                label="Dense padding"
            />
        </div>
    );
}
