/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.components.Header';

export default defineMessages({
  home: {
    id: `${scope}.home`,
    defaultMessage: 'Home',
  },
  features: {
    id: `${scope}.features`,
    defaultMessage: 'Features',
  },
  customers: {
    id: `${scope}.customers`,
    defaultMessage: 'Customers',
  },
  newProduct: {
    id: `${scope}.newProduct`,
    defaultMessage: 'newProduct',
  },
  orderDetails: {
    id: `${scope}.orderDetails`,
    defaultMessage: 'orderDetails',
  },
  orders: {
    id: `${scope}.orders`,
    defaultMessage: 'orders',
  },
  preferences: {
    id: `${scope}.preferences`,
    defaultMessage: 'preferences',
  },
  products: {
    id: `${scope}.products`,
    defaultMessage: 'products',
  },
  reports: {
    id: `${scope}.reports`,
    defaultMessage: 'reports',
  },
  settings: {
    id: `${scope}.settings`,
    defaultMessage: 'settings',
  },
  themes: {
    id: `${scope}.themes`,
    defaultMessage: 'themes',
  },
});
