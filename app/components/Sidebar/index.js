import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import messages from './messages';

export default function Sidebar() {
    return (
        <div className="mainsidenav">
            <ul>
                <li>
                    <Link to="/">
                        <FormattedMessage {...messages.home} />
                    </Link>
                </li>
                <li>
                    <Link to="/features">
                        <FormattedMessage {...messages.features} />
                    </Link>
                </li>
                <li>
                    <Link to="/customers">
                        <FormattedMessage {...messages.customers} />
                    </Link>
                </li>
                <li>
                    <Link to="/new-product">
                        <FormattedMessage {...messages.newProduct} />
                    </Link>
                </li>
                <li>
                    <Link to="/order-details">
                        <FormattedMessage {...messages.orderDetails} />
                    </Link>
                </li>
                <li>
                    <Link to="/orders">
                        <FormattedMessage {...messages.orders} />
                    </Link>
                </li>
                <li>
                    <Link to="/preferences">
                        <FormattedMessage {...messages.preferences} />
                    </Link>
                </li>
                <li>
                    <Link to="/products">
                        <FormattedMessage {...messages.products} />
                    </Link>
                </li>
                <li>
                    <Link to="/reports">
                        <FormattedMessage {...messages.reports} />
                    </Link>
                </li>
                <li>
                    <Link to="/settings">
                        <FormattedMessage {...messages.settings} />
                    </Link>
                </li>
            </ul>
        </div>
    );
}
