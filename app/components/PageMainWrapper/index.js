import React from 'react';

export default function PageMainWrapper(props) {
    console.log(props);
    return <div className={props.class} >
        {props.children}
    </div>;
}