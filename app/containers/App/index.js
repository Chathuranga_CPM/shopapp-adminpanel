/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import HomePage from 'containers/Pages/HomePage/Loadable';
import FeaturePage from 'containers/Pages/FeaturePage/Loadable';
import NotFoundPage from 'containers/Pages/NotFoundPage/Loadable';
import CustomerPage from 'containers/Pages/CustomerPage/Loadable';
import NewProductPage from 'containers/Pages/NewProductPage/Loadable';
import OrderDetailPage from 'containers/Pages/OrderDetailPage/Loadable';
import OrdersPage from 'containers/Pages/OrdersPage/Loadable';
import PreferencePage from 'containers/Pages/PreferencePage/Loadable';
import ProductPage from 'containers/Pages/ProductPage/Loadable';
import ReportPage from 'containers/Pages/ReportPage/Loadable';
import SettingsPage from 'containers/Pages/SettingsPage/Loadable';
import ThemesPage from 'containers/Pages/ThemesPage/Loadable';
import Header from 'components/Header';
import Sidebar from 'components/Sidebar';

import '../../assets/css/core.min.css';

const AppWrapper = styled.div``;

export default function App() {
    return (
        <AppWrapper>
            <Helmet
                titleTemplate="%s - Shop app"
                defaultTitle="Shop app"
            >
                <meta
                    name="description"
                    content="A Shop application"
                />
            </Helmet>
            <Header />
            <Sidebar />

            <div className="container">
                <div className="dataappendarea">
                    <Switch>
                        <Route exact path="/" component={HomePage} />
                        <Route path="/features" component={FeaturePage} />
                        <Route path="/customers" component={CustomerPage} />
                        <Route path="/new-product" component={NewProductPage} />
                        <Route path="/order-details" component={OrderDetailPage} />
                        <Route path="/orders" component={OrdersPage} />
                        <Route path="/preferences" component={PreferencePage} />
                        <Route path="/products" component={ProductPage} />
                        <Route path="/reports" component={ReportPage} />
                        <Route path="/settings" component={SettingsPage} />
                        <Route path="/themes" component={ThemesPage} />
                        <Route path="" component={NotFoundPage} />
                    </Switch>
                </div>
            </div>
            
            {/* <Footer /> */}
        </AppWrapper>
    );
}
