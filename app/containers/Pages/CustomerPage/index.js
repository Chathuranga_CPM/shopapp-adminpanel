import React from 'react';
import CustomPageHeader from '../../../components/CustomPageHeader';
import PageMainWrapper from '../../../components/PageMainWrapper';
import ListCustomers from '../../../components/ListCustomers';

import './css/customers.min.css';

export default function CustomerPage() {
    return (
        <div>
            <CustomPageHeader title="Customers" />

            <PageMainWrapper class="customerswrapper" setBackgroundImg>
                <div className="custwrapinner">
                    <ListCustomers />
                </div>
            </PageMainWrapper>
        </div>
    );
}
