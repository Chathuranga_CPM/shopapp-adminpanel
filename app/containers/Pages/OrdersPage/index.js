import React from 'react';
import MaterialTable from 'material-table';

export default function OrdersPage() {
    return (
        <MaterialTable
            title="Editable Preview"
            columns={[
                { title: 'Name', field: 'name' },
                {
                    title: 'Surname',
                    field: 'surname',
                    initialEditValue: 'initial edit value',
                },
                { title: 'Birth Year', field: 'birthYear', type: 'numeric' },
                {
                    title: 'Birth Place',
                    field: 'birthCity',
                    lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' },
                },
            ]}
            data={[
                {
                    name: 'Mehmet',
                    surname: 'Baran',
                    birthYear: 1987,
                    birthCity: 63,
                },
                {
                    name: 'Zerya Betül',
                    surname: 'Baran',
                    birthYear: 2017,
                    birthCity: 34,
                },
            ]}
            editable={{
                onRowAdd: newData =>
                    // eslint-disable-next-line no-unused-vars
                    new Promise((resolve, reject) => {
                        setTimeout(() => {
                            {
                                const { data } = this.state;
                                data.push(newData);
                                this.setState({ data }, () => resolve());
                            }
                            resolve();
                        }, 1000);
                    }),
                onRowUpdate: (newData, oldData) =>
                    // eslint-disable-next-line no-unused-vars
                    new Promise((resolve, reject) => {
                        setTimeout(() => {
                            {
                                const { data } = this.state;
                                const index = data.indexOf(oldData);
                                data[index] = newData;
                                this.setState({ data }, () => resolve());
                            }
                            resolve();
                        }, 1000);
                    }),
                onRowDelete: oldData =>
                    // eslint-disable-next-line no-unused-vars
                    new Promise((resolve, reject) => {
                        setTimeout(() => {
                            {
                                const { data } = this.state;
                                const index = data.indexOf(oldData);
                                data.splice(index, 1);
                                this.setState({ data }, () => resolve());
                            }
                            resolve();
                        }, 1000);
                    }),
            }}
        />
    );
}
