import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Select from '@material-ui/core/Select';
import Switch from '@material-ui/core/Switch';
import CustomPageHeader from '../../../components/CustomPageHeader';
import PageMainWrapper from '../../../components/PageMainWrapper';



const useStyles = makeStyles((theme) => ({
    maintitle: {
        fontSize:20,
        marginBottom:20,
        fontWeight:'bold'
    },
    marginbottom:{
        marginBottom:25,
    },
    paddingset: {
        padding:45,
        borderRadius:7,
        marginBottom:35
    },
    fullwidth: {
        width:'100%'
    },
  })
);



export default function NewProductPage() {
    const classes = useStyles();
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
    });
    
    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const [state, setState] = React.useState({
        checkedA: true,
        checkedB: true,
    });

    const handleChangeSwitch = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };

    
    
    return <div><CustomPageHeader title="Add new product" />

    <PageMainWrapper class="addproductwrapper" setBackgroundImg>
        <div className="centerdata">
            <Grid container spacing={3}>
                <Grid item xs={12} sm={8}>
                    <Paper className={classes.paddingset} variant="outlined">
                        <Typography className={classes.maintitle}  variant="h5" component="h2" color="textPrimary" gutterBottom>
                            General
                        </Typography>
                        <Typography variant="p"  className={classes.marginbottom} component="p" color="textSecondary" gutterBottom>
                            This is some descriptions about this card
                        </Typography>
                        <TextField
                            id="outlined-full-width"
                            label="Product name"
                            fullWidth
                            variant="outlined"
                            className={classes.marginbottom}
                        />

                        <TextField
                            id="outlined-textarea"
                            label="Product descriptions"
                            multiline
                            fullWidth
                            variant="outlined"
                            rows="4"/>
                    </Paper>
                    <Paper className={classes.paddingset} variant="outlined">
                        <Typography className={classes.maintitle}  variant="h5" component="h2" color="textPrimary" gutterBottom>
                            Product images
                        </Typography>
                        
                    </Paper>
                    <Paper className={classes.paddingset} variant="outlined">
                        <Typography className={classes.maintitle}  variant="h5" component="h2" color="textPrimary" gutterBottom>
                            Prices
                        </Typography>
                        <Grid container spacing={3} className={classes.marginbottom}>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth  variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-amount">Min Price</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-amount"
                                        value={values.amount}
                                        onChange={handleChange('amount')}
                                        startAdornment={<InputAdornment position="start">Rs :</InputAdornment>}
                                        labelWidth={60}
                                    />
                                
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth  variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-amount">Max Price</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-amount"
                                        value={values.amount}
                                        onChange={handleChange('amount')}
                                        startAdornment={<InputAdornment position="start">Rs :</InputAdornment>}
                                        labelWidth={60}
                                    />
                                
                                </FormControl>
                            </Grid>
                        </Grid>
                        <FormControlLabel
                            control={
                            <Switch
                                checked={state.checkedB}
                                onChange={handleChangeSwitch}
                                name="checkedB"
                                color="primary"
                            />
                            }
                            label="Charge taxes on this product"
                        />
                        
                    </Paper>
                    <Paper className={classes.paddingset} variant="outlined">
                        <Typography  className={classes.maintitle}  variant="h5" component="h2" color="textPrimary" gutterBottom>
                            Inventory
                        </Typography>
                        <Grid container spacing={3} className={classes.marginbottom}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="outlined-full-width"
                                    label="SKU (Stock Keeping Unit)"
                                    fullWidth
                                    variant="outlined"
                                    className={classes.marginbottom}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="outlined-full-width"
                                    label="Barcode (ISBN, UPC, GTIN, etc.)"
                                    fullWidth
                                    variant="outlined"
                                    className={classes.marginbottom}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                
                                 <FormControl variant="outlined" className={classes.fullwidth}>
                                    <InputLabel htmlFor="outlined-inventorypolicy-native-simple">Inventory policy</InputLabel>
                                    <Select
                                        native
                                        value={state.age}
                                        onChange={handleChange}
                                        label="Inventory policy"
                                        className={classes.fullwidth}
                                        inputProps={{
                                            name: 'inventorypolicy',
                                            id: 'outlined-inventorypolicy-native-simple',
                                        }}
                                    >
                                        <option aria-label="None" value="" />
                                        <option value={10}>Ten</option>
                                        <option value={20}>Twenty</option>
                                        <option value={30}>Thirty</option>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="outlined-full-width"
                                    label="Quantity"
                                    fullWidth
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <FormControlLabel
                            control={
                            <Switch
                                checked={state.checkedB}
                                onChange={handleChangeSwitch}
                                name="checkedB"
                                color="primary"
                            />
                            }
                            label="Allow customers to purchase this product when it's out of stock"
                        />
                        
                    </Paper>
                    <Paper className={classes.paddingset} variant="outlined">
                        <Typography  className={classes.maintitle}  variant="h5" component="h2" color="textPrimary" gutterBottom>
                            Shipping
                        </Typography>

                        <Grid container spacing={3} className={classes.marginbottom}>
                            <Grid item xs={12} sm={7}>
                                <FormControlLabel
                                    control={
                                    <Switch
                                        checked={state.checkedB}
                                        onChange={handleChangeSwitch}
                                        name="checkedB"
                                        color="primary"
                                    />
                                    }
                                    label="This product requires shipping"
                                />
                            </Grid>
                            <Grid item xs={12} sm={5}>
                                <FormControlLabel
                                    control={
                                    <Switch
                                        checked={state.checkedB}
                                        onChange={handleChangeSwitch}
                                        name="checkedB"
                                        color="primary"
                                    />
                                    }
                                    label="Cash on delivery"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                
                                 <FormControl variant="outlined" className={classes.fullwidth}>
                                    <InputLabel htmlFor="outlined-inventorypolicy-native-simple">Inventory policy</InputLabel>
                                    <Select
                                        native
                                        value={state.age}
                                        onChange={handleChange}
                                        label="Inventory policy"
                                        className={classes.fullwidth}
                                        inputProps={{
                                            name: 'inventorypolicy',
                                            id: 'outlined-inventorypolicy-native-simple',
                                        }}
                                    >
                                        <option aria-label="None" value="" />
                                        <option value={10}>Ten</option>
                                        <option value={20}>Twenty</option>
                                        <option value={30}>Thirty</option>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="outlined-full-width"
                                    label="Quantity"
                                    fullWidth
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        
                        
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={4}>
                    <Paper>xs=6</Paper>
                </Grid>
            </Grid>
        </div>
    </PageMainWrapper></div>;
}